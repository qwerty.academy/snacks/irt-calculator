import tabela from "./tabela-2020.db.js";

export function encontrarCategoria(sujeito) {
  return tabela.find((c) => c.minimo < sujeito && c.maximo >= sujeito);
}
