import { encontrarCategoria } from "./auxiliares.js";

describe("Funções auxiliares", () => {
  describe("encontrarCategoria()", () => {
    test("Devolve a primeira categoria quando o montante sujeito menor ou igual a 70 000 Kz", () => {
      const sujeito = 20000;
      const categoria = encontrarCategoria(sujeito);

      expect(categoria.fixo).toEqual(0);
      expect(categoria.maximo).toEqual(70000);
      expect(categoria.percentagem).toEqual(0);
    });

    test("Devolve a fixo=292250 percentagem=0.22 quando o montante sujeito entre 1 500 001 e 2 000 000", () => {
      const sujeito = 1850000;
      const categoria = encontrarCategoria(sujeito);

      expect(categoria.fixo).toEqual(292250);
      expect(categoria.percentagem).toEqual(0.22);
    });
  });
});
